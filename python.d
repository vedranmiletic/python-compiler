#! /usr/bin/env rdmd

import std.exception;
import std.stdio;
import std.file;
import std.process;

void main (string[] arguments) {
  
  enforce(arguments.length == 2);
  auto pythonCode = cast(string) std.file.read(arguments[1]);

  auto tokens = tokenize(pythonCode);
  writeln(tokens);
  auto filteredTokens = tokenFilter(tokens);
  writeln(filteredTokens);
  auto ast = parse(filteredTokens);
  writeln(ast);
  writeln(ast.generate());
  //return;
  
  auto dCode = ast.generate();
  //auto dCode = generate_(tokenize(pythonCode));
  
  auto tempPath = std.file.tempDir ~ "/python_compiler_h1af3ijejivxkpqrduifgjbzss5gcfg37ksbjnrluzyro5f";
  if (std.file.exists(tempPath))
    rmdirRecurse(tempPath);
  mkdirRecurse(tempPath);
  std.file.write(tempPath ~ "/program.d", dCode);
  
  //auto cwd = getcwd();
  //chdir(tempPath);
  //scope(exit) chdir(cwd);
  
  system("dmd -run " ~ tempPath ~ "/program.d");

}

enum tokenType {
  whitespace,
  ident,
  statement,
  symbol,
  literal,
}

struct token {
  tokenType type;
  string value;
}

token[] tokenize (string input) {
  
  import std.string;
  import std.regex;

  token[] tokens;
  int offset = 0;
  string remaining = input;
  
  while (remaining.length > 0) {

    if (strip(remaining).length == 0) {
      tokens ~= token(tokenType.whitespace, remaining);
      remaining = "";
      continue;
    }

    if (strip(remaining[0 .. indexOf(remaining, "\n") != -1 ? indexOf(remaining, "\n") + 1 : $]) == "") {
      tokens ~= token(tokenType.whitespace, remaining[0 .. indexOf(remaining, "\n") != -1 ? indexOf(remaining, "\n") + 1 : $]);
      remaining = remaining[tokens[$ - 1].value.length .. $];
      if (remaining.length > stripLeft(remaining).length) {
        tokens ~= token(tokenType.ident, remaining[0 .. remaining.length - stripLeft(remaining).length]);
        remaining = remaining[remaining.length - stripLeft(remaining).length .. $];
      }      
      continue;
    }
    
    if (remaining.length > stripLeft(remaining).length) {
      tokens ~= token(tokenType.whitespace, remaining[0 .. remaining.length - stripLeft(remaining).length]);
      remaining = remaining[remaining.length - stripLeft(remaining).length .. $];
      continue;
    }

    auto symbolRegex = ctRegex!r"^[a-zA-Z_][a-zA-Z0-9_]*";
    auto m = match(remaining, symbolRegex);
    if (m.captures.length > 0) {
      switch (m.captures[0]) {
        case "if": case "while": case "for":
          tokens ~= token(tokenType.statement, m.captures[0]);
          break;
        default:
          tokens ~= token(tokenType.symbol, m.captures[0]);
      }
      remaining = remaining[m.captures[0].length .. $];
      continue;
    }

    auto specialSymbols = [":", ",", ";"];
    bool found = false;
    foreach (s; specialSymbols) {
      if (remaining.length >= s.length && remaining[0 .. s.length] == s) {
        tokens ~= token(tokenType.symbol, s);
        remaining = remaining[s.length .. $];
        found = true;
        break;
      }
    }
    if (found)
      continue;

    auto literalRegex = ctRegex!`^"(\\\\.|[^\\\\"])*"`;
    auto m2 = match(remaining, literalRegex);
    if (m2.captures.length > 0) {
      tokens ~= token(tokenType.literal, m2.captures[0]);
      remaining = remaining[m2.captures[0].length .. $];
      continue;
    }
 
    debug writeln("Tokens: ", tokens);
    debug writeln("Remaining: ", remaining);
    throw new Exception("Syntax error");
  }

  //writeln(remaining);
  
  return tokens;
}

auto tokenFilter (token[] tokens) {
  token[] result;

  foreach (t; tokens) {
    if (t.type == tokenType.whitespace)
      continue;
    result ~= t;
  }

  return result;
}



struct ast {
  astEntry[] entries;
}

struct astEntry {
  enum types {
    printStatement,
    ifStatement,
    whileStatement,
  }
  types type;
  astExpression[] arguments;
  ast body_;

  this (types type, string s) {
    this.type = type;
    this.arguments ~= astExpression(s);
  }

}

struct astExpression {
  enum types { string_, int_, }

  types type;
  union {
    string stringValue;
    int intValue;
  }

  this (string s) {
    this.type = types.string_;
    this.stringValue = s;
  }

}

auto parse (token[] tokens) {

  int offset = 0;
  ast result;

  for (offset = 0; offset < tokens.length; offset++) {
    auto token = tokens[offset];
    if (token.type == tokenType.statement) {
      if (token.value == "if") {
        enforce(tokens[offset + 2].type == tokenType.symbol && tokens[offset + 2].value == ":", "Syntax error");
        result.entries ~= astEntry(astEntry.types.ifStatement, tokens[offset + 1].value);
        offset += 2;
        // todo:
        //parse(tokens[offset .. $], 
      }
    }
    if (token.type == tokenType.symbol) {
      if (token.value == "print") {
        assert(tokens[offset + 1].type == tokenType.literal);
        result.entries ~= astEntry(astEntry.types.printStatement, tokens[offset + 1].value);
        offset++;
      }
    } else {
      throw new Exception("Can't handle token");
    }

  }

  return result;
}


string generate (ast source) {
  string dCode = "import std.stdio;\n\nvoid main () {\n";

  foreach (entry; source.entries)
    dCode ~= entry.generate();
  
  dCode ~= "\n}\n";

  return dCode;
}

string generate (astEntry source) {
  switch (source.type) {
    case astEntry.types.printStatement:
      return "writeln(" ~ source.arguments[0].generate() ~ ");\n";
      break;
    default:
      assert(false);
  }
  return ";";
}

string generate (astExpression source) {
  switch (source.type) {
    case astExpression.types.string_:
      return source.stringValue;
    default:
      assert(false);
  }
}













string generate_ (token[] tokens) {
  int offset = 0;
  string dCode = "import std.stdio;\n\nvoid main () {\n";
  
  for (offset = 0; offset < tokens.length; offset++) {
    auto token = tokens[offset];
    if (token.type == tokenType.whitespace)
      continue;
    if (token.type == tokenType.symbol) {
      if (token.value == "print") {
        if (tokens[offset + 1].type == tokenType.whitespace)
          offset++;
        assert(tokens[offset + 1].type == tokenType.literal);
        dCode ~= "writeln(" ~ tokens[offset + 1].value ~ ");\n";
        offset++;
      }
    } else {
      throw new Exception("Can't handle token");
    }

  }
  
  dCode ~= "\n}\n";

  return dCode;
}


